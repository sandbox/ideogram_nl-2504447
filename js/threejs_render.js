var container;

var camera, scene, renderer;

var mouseX = 0, mouseY = 0;

var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;
var aspect = window.innerHeight / window.innerWidth;

var objects = [];
var textures = [];

var lights = [];

var radians = 0.01745;

init();
animate();


function init() {

    // GET VARIABLES FROM DRUPAL

    zenithColor = Drupal.settings.threejs_scene.zenith.replace("#","0x");
    horizonColor = Drupal.settings.threejs_scene.horizon.replace("#","0x");
    floorColor = Drupal.settings.threejs_scene.floor.replace("#","0x");

    // HTML work

    container = document.createElement('div');
    container.id = 'ThreeJS';
    elements = document.body.getElementsByClassName('field-type-threejs-object');
    element = elements[0];
    element.appendChild(container);

    // SETUP

    // setup - CAMERA

    camera = new THREE.PerspectiveCamera(45, 1 / aspect, 1, 5000);
    camera.position.set(0,0,100);

    // SCENE

    scene = new THREE.Scene();

    /* scene setup thanks to mrdoob: http://mrdoob.github.io/three.js/examples/webgl_lights_hemisphere.html */

    // scene - LIGHTS

    // scene - lights - HEMILIGHT

    hemiLight = new THREE.HemisphereLight( 0xffffff, 0xffffff, 0.6 );
    hemiLight.color.setHex(zenithColor);
    hemiLight.groundColor.setHSL( 0.095, 1, 0.75 );
    hemiLight.position.set( 0, 500, 0 );
    scene.add( hemiLight );

    // scene - lights - DIRECTIONAL LIGHT

    dirLight = new THREE.DirectionalLight( 0xffffff, 1 );
    dirLight.color.setHSL( 0.1, 1, 0.95 );
    dirLight.position.set( -1, 1.75, 1 );
    dirLight.position.multiplyScalar( 5000 );
    scene.add( dirLight );

    dirLight.castShadow = true;

    dirLight.shadowMapWidth = 2048;
    dirLight.shadowMapHeight = 2048;

    var d = 50;

    dirLight.shadowCameraLeft = -d;
    dirLight.shadowCameraRight = d;
    dirLight.shadowCameraTop = d;
    dirLight.shadowCameraBottom = -d;

    dirLight.shadowCameraFar = 3500;
    dirLight.shadowBias = -0.0001;
    dirLight.shadowDarkness = 0.35;
    dirLight.shadowCameraVisible = true;

    // scene - FOG

    scene.fog = new THREE.Fog( 0xffffff, 1, 5000 );
    scene.fog.color.setHSL( 0.6, 0, 1 );

    // scene - GROUND

    var groundGeo = new THREE.PlaneBufferGeometry( 10000, 10000 );
    var groundMat = new THREE.MeshPhongMaterial( { color: 0xffffff, specular: 0x050505 } );
    groundMat.color.setHex ( floorColor );

    var ground = new THREE.Mesh( groundGeo, groundMat );
    ground.rotation.x = -Math.PI/2;
    ground.position.y = 0;
    scene.add( ground );

    ground.receiveShadow = true;

    // scene - SKYDOME

    var vertexShader = document.getElementById( 'vertexShader' ).textContent;
    var fragmentShader = document.getElementById( 'fragmentShader' ).textContent;
    var uniforms = {
        topColor: 	 { type: "c", value: new THREE.Color( 0xffffff ) },
        bottomColor: { type: "c", value: new THREE.Color( parseInt( horizonColor ) ) },
        offset:		 { type: "f", value: 33 },
        exponent:	 { type: "f", value: 0.6 }
    }

    uniforms.topColor.value.copy( hemiLight.color ); // same as 'zenithColor.

    scene.fog.color.setHex( horizonColor );

    var skyGeo = new THREE.SphereGeometry( 4000, 32, 15 );
    var skyMat = new THREE.ShaderMaterial( { vertexShader: vertexShader, fragmentShader: fragmentShader, uniforms: uniforms, side: THREE.BackSide } );

    var sky = new THREE.Mesh( skyGeo, skyMat );
    scene.add( sky );


    // setup - RENDERER

    renderer = new THREE.WebGLRenderer();
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(container.offsetWidth, container.offsetWidth * aspect);
    container.appendChild(renderer.domElement);


    // setup -  CONTROLS
    controls = new THREE.OrbitControls(camera, renderer.domElement);

    // setup - HELPERS

    //scene.add(new THREE.AxisHelper(110));
    //scene.add(new THREE.GridHelper(100, 10));

    // setup - LOAD MANAGER

    var manager = new THREE.LoadingManager();
    manager.onProgress = function (item, loaded, total) {

        console.log(item, loaded, total);

        if (loaded == total) {
            scene_loaded();
        }

    };

    var onProgress = function (xhr) {
        if (xhr.lengthComputable) {
            var percentComplete = xhr.loaded / xhr.total * 100;
            //console.log(Math.round(percentComplete, 2) + '% downloaded');
        }
    };

    var onError = function (xhr) {
    };

    // MODEL

    // model - LOAD TEXTURE

    for (var i = 0; i < Drupal.settings.threejs.length; i++) {

        var loader = new THREE.ImageLoader(manager);

        str_material_uvmap_url = Drupal.settings.threejs[i].material_uvmap;

        loader.load(str_material_uvmap_url, (function (url, index) {
                return function (image, i) {
                    textures[index] = new THREE.Texture();
                    textures[index].image = image;
                    textures[index].needsUpdate = true;
                }
            })(str_material_uvmap_url, i)
            , onProgress
            , onError
        );
    }

    // model - LOAD OBJECT

    var loader = new THREE.OBJLoader(manager);

    for (var i = 0; i < Drupal.settings.threejs.length; i++) {

        str_model_url = Drupal.settings.threejs[i].geometry;

        loader.load(str_model_url, (function (url, index) {
                return function (object, i) {

                    objects[index] = object;

                    // apply texture
                    objects[index].traverse(function (child) {

                        if (child instanceof THREE.Mesh) {
                            child.material.map = textures[index];
                            child.material.side = THREE.DoubleSide;
                            console.log(child);
                        }
                    });

                    scene.add(objects[index]);

                    // apply transform
                    objects[index].position.x = parseInt(Drupal.settings.threejs[index].position.x);
                    objects[index].position.y = parseInt(Drupal.settings.threejs[index].position.y);
                    objects[index].position.z = parseInt(Drupal.settings.threejs[index].position.z);

                    objects[index].rotation.x = parseInt(Drupal.settings.threejs[index].rotation.x) * radians;
                    objects[index].rotation.y = parseInt(Drupal.settings.threejs[index].rotation.y) * radians;
                    objects[index].rotation.z = parseInt(Drupal.settings.threejs[index].rotation.z) * radians;

                    objects[index].scale.x = parseInt(Drupal.settings.threejs[index].geometry_scale);
                    objects[index].scale.y = parseInt(Drupal.settings.threejs[index].geometry_scale);
                    objects[index].scale.z = parseInt(Drupal.settings.threejs[index].geometry_scale);

                }
            })(str_model_url, i)
            , onProgress
            , onError
        );
    }
}


function scene_loaded() {

    // Point the camera at the first object given by Drupal
    // (For the moment, this is as good a choice as any...)


    var subject = objects[0];

    var helper = new THREE.BoundingBoxHelper(subject);
    helper.update();

    // get the bounding sphere
    var boundingSphere = helper.box.getBoundingSphere();

    // calculate the distance from the center of the sphere
    // and subtract the radius to get the real distance.

    var center = boundingSphere.center;
    var radius = boundingSphere.radius;
    var distance = center.distanceTo(camera.position) - radius;
    var realHeight = Math.abs(helper.box.max.y - helper.box.min.y);

    // Make the camera look at the subject
    controls.center = center;

    camera.position.lerpVectors(
        camera.position,
        center,
        realHeight / distance
    );


}

function onWindowResize() {

    windowHalfX = window.innerWidth / 2;
    windowHalfY = window.innerHeight / 2;

    //camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);
}

function onDocumentMouseMove(event) {

    mouseX = ( event.clientX - windowHalfX ) / 2;
    mouseY = ( event.clientY - windowHalfY ) / 2;

}

//

function animate() {
    requestAnimationFrame(animate);
    render();
    update();
}

function update() {
    controls.update();

}

function render() {

    renderer.render(scene, camera);

}

// These functions control the rotation and navigation

console.log( Drupal.settings );

var angle_inc = 6.28 / 24;
jQuery("#ThreeJS")
    .on("mousedown", function () {
        waiting_for_up = true;
        setTimeout(function () {
            waiting_for_up = false;
        }, 500)
    })
    .on("mouseup", function () {
        if (waiting_for_up) {
            controls.autoRotate = !controls.autoRotate;
        }
    });

jQuery("#panUp").on("click", function () {
    controls.pan(new THREE.Vector3(0, -10, 0))
});
jQuery("#panDown").on("click", function () {
    controls.pan(new THREE.Vector3(0, +10, 0))
});
jQuery("#panLeft").on("click", function () {
    controls.pan(new THREE.Vector3(-10, 0, 0))
});
jQuery("#panRight").on("click", function () {
    controls.pan(new THREE.Vector3(+10, 0, 0))
});
jQuery("#rotZmin").on("click", function () {
    controls.rotateRight(angle_inc)
});
jQuery("#rotZplus").on("click", function () {
    controls.rotateLeft(angle_inc)
});
jQuery("#rotXmin").on("click", function () {
    controls.rotateUp(angle_inc)
});
jQuery("#rotXplus").on("click", function () {
    controls.rotateDown(angle_inc)
});
jQuery("#zoomIn").on("click", function () {
    controls.zoomOut(1.1)
});
jQuery("#zoomOut").on("click", function () {
    controls.zoomIn(1.1)
});